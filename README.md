# web_platform_mvp_ver_2

This is second attempt at creating a web - based mvp

# Rules
1. Để bảo vệ Source Code (x2). Thực hiện các bước sau:
	* B1: tạo một branch local: git checkout -b my-new-local-branch
	* B2: commit thay đổi
	* B3: git push origin my-new-local-branch
	* B4: ấn vào đường link hiện ra để tạo một merge request
	![create merge request](picture/create_merge_request.png)
	* B5: chỉnh sửa merge request để __MERGE VÀO BRANCH DEVELOP__
	![Finalize merge request](picture/finalize_merge_request.png)
	
1. Remember to comment code clearly so that one can understand the structure of code quickly.

# File structure
1. __domain_logic__: domain_logic chứa các logic liên quan tới nghiệp vụ business
    * Buyer.js: chứa các logic liên quan tới Buyer
        * xem được hàng (CarbonNFT) Supplier đăng lên theo một danh sách (query)
        * kết nối tới ví
        * mua hàng bằng coin trong ví
        * xem mặt hàng (CarbonCredit) đã mua
    * Supplier.js: chứa các logic liên quan tới Supplier
        * đăng hàng (CarbonNFT) lên moralis DB (đăng hàng lên marketplace)
        * gỡ hàng (CarbonNFT) khỏi moralis DB
        * cập nhật trạng thái đơn hàng (CarbonNFT) (updated)
        * xem danh sách hàng (CarbonNFT)
        * xử lý đơn hàng (CarbonNFT)
    * Operator.js: chứa các logic liên quan tới Operator
    * common: file chứa các logic chung (generic)
        * Wallet.js: chứa các logic liên quan đến Wallet
            * Login/ logout tới ví
            * lấy giá trị tên tài khoản
            * Kiểm tra đã đăng nhập chưa
        * CarbonNFTHolder.js:
            * kiểm tra trạng thái của hàng
            * update trạng thái của hàng
        * State.js: chứa trạng thái của hàng
        * Common.js: chứa các function tiện ích
        * Chain.js: trao đổi với chain
        * Event.js: quản lý EventEmitter

2. __domain_contract__: chứa các contract xử lý giao dịch on - chain.

# Design
1. Design for domain_logic can be found at: https://github.com/SETS-VN/ITTeamManagement/tree/main/Product%20Description/Platform/Web%20Platform%20ver%202

# Testing
file main.js là file nháp được sử dụng để thử nghiệm các đoạn code đã viết. Coder thích thay đổi main.js thế nào cũng được.

để chạy thử nghiệm:
```
node main.js
```

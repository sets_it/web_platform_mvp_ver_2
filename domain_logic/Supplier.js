const Moralis = require("moralis/node");
const CarbonNFTHolder = require("./common/CarbonNFTHolder.js");
const isOwnerOf = require("./common/Common.js").isOwnerOf;
const Wallet = require("./common/Wallet.js");
const STATE = require("./common/State.js");
const Event = require("./common/Event.js");
const EventType = Event.EventType;
const Chain = require("./common/Chain.js");

require("dotenv").config();

Moralis.initialize(
  process.env.MORALIS_APPLICATION_ID,
  process.env.MORALIS_MASTER_KEY
);
Moralis.serverURL = process.env.MORALIS_SERVER_URL;

const UserDB = Moralis.Object.extend("UserDB");
const query = new Moralis.Query(UserDB);

/**
 * Supplier include:
 * 1. load/unload CarbonNFT
 * 2. UserDB
 * 3. Event
 */

module.exports = class Supplier {
  #wallet;
  #NFT_dict;
  #NFT_NAME_list;
  #user;
  #eventEmitter;

  /**
   * construct Supplier and return instance of Supplier
   * @param {Wallet} wallet wallet instance of Supplier
   */
  static async getSupplier(wallet) {
    //wallet must be connected before entering into Supplier
    wallet.isWalletConnected();

    let supplier = new Supplier();

    supplier.#wallet = wallet;

    // get supplier NFT. getNFTs is an async so I will let it go loose first.
    supplier.#NFT_NAME_list = Chain.getNFTs(supplier.#wallet.getAddress());

    // get UserDB from Moralis DB
    query.equalTo("wallet_address", supplier.#wallet.getAddress());
    supplier.#user = await query.first();

    // construct UserDB
    if (!supplier.#user) {
      supplier.#user = new UserDB();

      supplier.#user.set("wallet_address", supplier.#wallet.getAddress());

      // save to Moralis DB
      supplier.#saveUserDB();
    }

    // get NFT lists from user's wallet 
    await supplier.#NFT_NAME_list;

    // get CarbonNFTHolder dict 
    supplier.#NFT_dict = new Object();
    supplier.#NFT_NAME_list.forEach((ele) => {
      supplier.#NFT_dict[ele.token_id] = await CarbonNFTHolder.getCarbonNFT(
        ele.token_id
      );
    });

    // register an EventEmitter
    supplier.#eventEmitter = new Event();
    await supplier.#registerCarbonNFTListener();

    return supplier;
  }

  //=============== 1. load/unload ===============

  /**
   * load CarbonNFT for sell
   * @param {String} nft_code identifier code of NFT
   * @param {String} price the price for NFT
   */
  async loadNFTforSell(nft_code, price) {
    // check if nft_code existed in NFT_dict
    if (typeof this.#NFT_dict[nft_code] === "undefined")
      throw new Error("No such NFT in Supplier ownership record");

    // check if nft_code is BURNABLE
    if (!this.#NFT_dict[nft_code].getState() === STATE.BURNABLE)
      throw new Error("NFT is not in wallet to up for sell");

    // update NFT state and price for sale
    this.#NFT_dict[nft_code].updateState(STATE.BUYABLE);
    this.#NFT_dict[nft_code].updatePrice(price);

    await this.#NFT_dict[nft_code].saveCarbonNFT();

    // emit event for Supplier front - end to receive
    this.#eventEmitter.emit(EventType.RELOAD_SUPPLIER_LIST, nft_code);

    // emit event for marketplace front - end to receive
    Event.getEventEmitter().emit(
      EventType.RELOAD_MARKET_LIST,
      this.#NFT_dict[nft_code]
    );
  }

  /**
   * unload CarbonNFT from DB
   * @param {String} nft_code the ID of NFT
   */
  async unloadNFTfromMarket(nft_code) {
    // check if nft_code existed in NFT_dict
    if (typeof this.#NFT_dict[nft_code] === "undefined") {
      throw new Error("No such NFT in Supplier ownership record");
    }

    // check if nft_code is BUYABLE
    if (this.#NFT_dict[nft_code].getState() != STATE.BUYABLE) {
        throw new Error("NFT is not buyable and can't be unloaded from the market");
    }

    // update NFT state and set price = 0
    this.#NFT_dict[nft_code].updateState(STATE.BURNABLE)
    this.#NFT_dict[nft_code].updatePrice(0)

    // update the state of the carbonNFT
    await this.#NFT_dict[nft_code].saveCarbonNFT()

    // emit event for Supplier front - end to receive
    this.#eventEmitter.emit(EventType.RELOAD_SUPPLIER_LIST, nft_code);

    // emit event for marketplace front - end to receive
    Event.getEventEmitter().emit(
        EventType.RELOAD_MARKET_LIST,
        this.#NFT_dict[nft_code]
    )
  }

  //=============== 2. UserDB ===============

  /**
   * save UserDB
   */
  async #saveUserDB() {
    let dataID = null;

    await this.#user
      .save()
      .then((data) => {
        dataID = data.id;
      })
      .catch((error) => {
        console.log(error);
      });

    return dataID;
  }

  /**
   * get container for CarbonNFTHolder
   * @returns an array of CarbonNFTHolder.js
   */
  getNFT_dict() {
    return this.#NFT_dict;
  }

  //=============== 3. Event ===============

  /**
   * register a listener on CarbonNFT
   */
  async #registerCarbonNFTListener() {
    let carbonNFTQuery = new Moralis.Query("CarbonNFT");
    let subscription = await carbonNFTQuery.subscribe();

    // subscribe for update on CarbonNFT
    subscription.on("update", this.#updateEventHandler);
  }

  /**
   * get EventEmitter to listen for Event
   */
  getEventEmitter() {
    return this.#eventEmitter;
  }

  /**
   * update event handler for moralis DB
   * @param {CarbonNFT} object
   */
  #updateEventHandler(object) {
    // filter to receive the right Object
    if (object.get("owner") != this.#wallet.getAddress()) return;

    let isChanged = false;

    // owner changed
    if (typeof this.#NFT_dict[object.get("nft_code")] === "undefined") {
      // create new CarbonNFTHolder
      this.#NFT_dict[object.get("nft_code")] = new CarbonNFTHolder(
        object.get("nft_code"),
        object.get("issuer"),
        object.get("owner"),
        object.get("emission_volume"),
        object.get("price"),
        object.get("state")
      );

      isChanged = true;

      // update CarbonNFT name to Supplier db list
      this.#NFT_NAME_list.push(object.get("nft_code"));
      this.#user.set("nft_list", this.#NFT_NAME_list);
      this.#saveUserDB();
    }

    // STATE changed
    if (
      object.get("state") != this.#NFT_dict[object.get("nft_code")].getState()
    ) {
      this.#NFT_dict[object.get("nft_code")].updateState(object.get("state"));

      isChanged = true;
    }

    // emit event to Supplier if there is change
    if (isChanged)
      this.#eventEmitter.emit(EventType.RELOAD_SUPPLIER_LIST, nft_code);
  }
};

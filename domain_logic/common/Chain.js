const Moralis = require('moralis/node');

require('dotenv').config();

Moralis.initialize(process.env.MORALIS_APPLICATION_ID, process.env.MORALIS_MASTER_KEY);
Moralis.serverURL = process.env.MORALIS_SERVER_URL;

/**
 * check if an address is owner of an NFT
 * @param {String} nft_code 
 * @param {String} address the address to be checked
 */
module.exports = class Chain {

    /**
     * @param {String} address address to be retrieved NFTs
     * @returns NFTs 
     */
    static async getNFTs(address){
        return Moralis.Web3API.account.getNFTsForContract({chain: process.env.CHAIN, address : address, token_address: process.env.NFT_CONTRACT_ADDRESS});
    }
}
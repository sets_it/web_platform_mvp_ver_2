const Moralis = require('moralis/node');

require('dotenv').config();

Moralis.initialize(process.env.MORALIS_APPLICATION_ID, process.env.MORALIS_MASTER_KEY);
Moralis.serverURL = process.env.MORALIS_SERVER_URL;

module.exports = class Wallet {

    #wallet;

    constructor(){
        this.#wallet = Moralis.User.current();
    }

    /**
     * connect to wallet
     */
    async connectToWallet() {
        if (!this.#wallet) {
            this.#wallet = await Moralis.Web3.authenticate();
        }
        console.log("wallet connected");
    }

    /**
     * log out of wallet
     */
    async logOut() {
        await Moralis.User.logOut().then(() => {
            // #wallet will now be null
            this.#wallet = Moralis.User.current();
        });
        console.log("wallet logged out");
    }

    /**
     * get address of wallet
     * @returns BSC address
     */
    getAddress(){
        this.isWalletConnected();

        return this.#wallet.get('bscAddress');
    }

    /**
     * check if wallet is connected
     */
    isWalletConnected(){
        if(this.#wallet) return;
        
        throw new Error("Need to connect to Wallet first");
    }

}
const EventEmitter = require('events');

module.exports = class Event extends EventEmitter{

    static #oneEventToRuleThemAll;

    /**
     * get global Event Emitter
     */
    static getEventEmitter(){
        if(typeof this.#oneEventToRuleThemAll === 'undefined'){
            this.#oneEventToRuleThemAll = new Event();
        }

        return this.#oneEventToRuleThemAll;
    }

}

module.exports.EventType = {
    RELOAD_SUPPLIER_LIST: "RELOAD_SUPPLIER_LIST",
    RELOAD_MARKET_LIST: "RELOAD_MARKET_LIST",
}
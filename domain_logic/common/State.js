module.exports = class STATE{
    static BUYABLE = "BUYABLE";
    static PROCESSABLE = "PROCESSABLE";
    static BURNABLE = "BURNABLE";
}
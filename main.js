const Moralis = require('moralis/node');

require('dotenv').config();

Moralis.initialize(process.env.MORALIS_APPLICATION_ID, process.env.MORALIS_MASTER_KEY);
Moralis.serverURL = process.env.MORALIS_SERVER_URL;
// do this to allow caching user login session
Moralis.User.enableUnsafeCurrentUser()

// Each event instance will only emit event and listen to event in its own instance.
const Event = require('./domain_logic/common/Event.js');

var event = new Event(); //local event

event.addListener('hello', (mess) => {
    console.log(mess);
})

// global event (getEventEmitter() actually follows Singleton)
Event.getEventEmitter().addListener('hello', (mess) => {
    console.log("this one is different: " + mess);
});

event.emit('hello', 'hello world'); // hello world

Event.getEventEmitter().emit('hello', 'hello world'); // this one is different: hello world